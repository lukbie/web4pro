import '../Hero/_hero.scss'
import './_style.scss'

export default function HeroAboutUs(props) {
  return(
    <div className="hero aboutUs__hero" style={{backgroundImage: 'url(/images/hero-au.jpg)', height: '100vh', backgroundSize: 'cover', marginTop: -93}}>
        <div className="container">
          <h1 className="hero__title">The right website development partner can <span>change everything</span></h1>
          <div className="hero__subtitle">We help tech companies to scale up by providing them with agile website development teams</div>
          <a href="#sectionStart" class="aboutUs__hero__more-button">
            <div class="aboutUs__hero__more-icon"></div> 
            <span class="aboutUs__hero__more-title"> Read our story </span> 
          </a>
        </div>
    </div>
  )
}