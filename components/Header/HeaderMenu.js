
import MenuLink from '../MenuLink'
import { PopupText } from "react-calendly"

const HeaderMenu = (props) => {
  const {showCta} = props
  
  return (
    <>
      {/* <MenuLink href="/services"><a>Services</a></MenuLink> */}
      {/* <MenuLink href="/services/" as="/uservices">Portfolio</MenuLink> */}
      <MenuLink href="/services/web-development"><a>Web Development</a></MenuLink>
      <MenuLink href="/services/website-design"><a>Website Design</a></MenuLink>
      <MenuLink href="/services/e-commerce"><a>E-commerce</a></MenuLink>
      <MenuLink href="/services/hosting-and-maintenance"><a>Hosting & Maintenance</a></MenuLink>
      {/* <MenuLink href="/about-us"><a>About Us</a></MenuLink> */}
      {/* <MenuLink href="/services/" as="/services">Blog</MenuLink> */}
      {/* <span onClick={props.showEstimate} className={`btn_cta ${ showCta ? '' : 'show-for-large'}`}>Start now</span> */}
      <span className={`btn_cta ${ showCta ? '' : 'show-for-large'}`}>
        <PopupText text="Start now" url="https://calendly.com/cleverwebsite/30min?primary_color=1a31f4&month=2020-09&back=1" />
      </span>
    </>
  )
}

export default HeaderMenu