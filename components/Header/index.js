import React, { useState, forwardRef, useImperativeHandle } from 'react'
import './_header.scss'
import './_hamburger.scss'
import './_moreMenu.scss'
import Link from "next/link"
import HeaderMenu from './HeaderMenu'
import LogoDark from '../../assets/image/LogoDark'
import LogoWhite from '../../assets/image/LogoWhite'
import { SERVICES } from '../../data/services'

const Header = (props) => {
  const [fullScreen, setFullScreen] = useState(false)
  const showHideMenu = () => {
    setFullScreen(!fullScreen)
  }
  process.browser ? fullScreen ? document.body.classList.add('nav-visible') : document.body.classList.remove('nav-visible') : null
  return (
    <>
    <div className="header">
      <div className="container">
        <div className="header__logo">
          <Link href="/"><a className="logoDark" aria-label="Home"><LogoDark/></a></Link>
          <Link href="/"><a className="logoWhite" aria-label="Home"><LogoWhite/></a></Link>
        </div>
        <div className="header__menu menu show-for-large">
          <HeaderMenu showEstimate={props.showEstimate}/>
        </div>

        <div style={{zIndex: 10, position: 'relative'}} class="header__secend-menu nav-button hide-for-large" aria-label="Swipe menu">
          <button onClick={showHideMenu} class="nav-button button-lines button-lines-x close nav-visible" aria-label="Swipe menu">
            <span class="lines"></span>
          </button>
        </div>
        
        <div className={fullScreen ? 'more-menu more-menu--active' : 'more-menu hide-for-large'}>
          <div className="container">
            <div className="more-menu__items">
              <HeaderMenu showEstimate={props.showEstimate} showCta={true}/>
            </div>
          </div>
        </div>

      </div>
    </div>
    {/* <div className="header__sec-menu">
      <div className="container">
        {SERVICES.map(service => (
          <div>
            <Link href={service.path}><a>
            {service.icon}
            <span>{service.name}</span>
            </a></Link>
          </div>
        ))}
      </div>
    </div> */}
    </>
  )
}

export default Header