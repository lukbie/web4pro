import './_cta.scss'
import { PopupText } from "react-calendly"

const Cta = (props) => {
  return (
    <div className="cta">
      <div className="container">
        <div className="cta__title">We’ve got you covered - combine our services and stop worrying about your website!</div>
        {/* <div className="cta__btn"><div onClick={props.showEstimate} className="btn_cta btn_cta--oposite">Start now</div></div> */}
        <div className="cta__btn"><div className="btn_cta btn_cta--oposite"><PopupText text="Start now" url="https://calendly.com/cleverwebsite/30min?primary_color=1a31f4&month=2020-09&back=1" /></div></div>
      </div>
    </div>
  )
}

export default Cta