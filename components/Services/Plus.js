import PlusIco from './images/IcoPlus'

export default function Plus(props) {
  return <div className="plus"><PlusIco /> </div>
}