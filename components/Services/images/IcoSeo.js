export default function IcoSeo() {
  return(
    <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M1 47H47" stroke="#D9D9D9" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M9 38C9 37.7348 8.89464 37.4804 8.70711 37.2929C8.51957 37.1054 8.26522 37 8 37H4C3.73478 37 3.48043 37.1054 3.29289 37.2929C3.10536 37.4804 3 37.7348 3 38V47H9V38Z" stroke="#D9D9D9" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M21 28C21 27.7348 20.8946 27.4804 20.7071 27.2929C20.5196 27.1054 20.2652 27 20 27H16C15.7348 27 15.4804 27.1054 15.2929 27.2929C15.1054 27.4804 15 27.7348 15 28V47H21V28Z" stroke="#D9D9D9" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M33 32C33 31.7348 32.8946 31.4804 32.7071 31.2929C32.5196 31.1054 32.2652 31 32 31H28C27.7348 31 27.4804 31.1054 27.2929 31.2929C27.1054 31.4804 27 31.7348 27 32V47H33V32Z" stroke="#D9D9D9" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M45 18C45 17.7348 44.8946 17.4804 44.7071 17.2929C44.5196 17.1054 44.2652 17 44 17H40C39.7348 17 39.4804 17.1054 39.2929 17.2929C39.1054 17.4804 39 17.7348 39 18V47H45V18Z" stroke="#D9D9D9" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M6 26C7.65685 26 9 24.6569 9 23C9 21.3431 7.65685 20 6 20C4.34315 20 3 21.3431 3 23C3 24.6569 4.34315 26 6 26Z" stroke="#D9D9D9" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M18 16C19.6569 16 21 14.6569 21 13C21 11.3431 19.6569 10 18 10C16.3431 10 15 11.3431 15 13C15 14.6569 16.3431 16 18 16Z" stroke="#D9D9D9" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M30 20C31.6569 20 33 18.6569 33 17C33 15.3431 31.6569 14 30 14C28.3431 14 27 15.3431 27 17C27 18.6569 28.3431 20 30 20Z" stroke="#D9D9D9" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M42 7C43.6569 7 45 5.65685 45 4C45 2.34315 43.6569 1 42 1C40.3431 1 39 2.34315 39 4C39 5.65685 40.3431 7 42 7Z" stroke="#D9D9D9" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M8.52199 20.9L15.696 14.92" stroke="#D9D9D9" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M20.848 13.948L27.154 16.05" stroke="#D9D9D9" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M39.6 5.79999L32.082 14.57" stroke="#D9D9D9" stroke-miterlimit="10" stroke-linecap="round"/>
    </svg>
  )
}