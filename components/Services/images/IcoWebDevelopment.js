function WebDevelopmentIco() {
  return (
    <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" clip-rule="evenodd" d="M32 47H16L17.6 38H30.4L32 47Z" stroke="#D9D9D9" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    <path fill-rule="evenodd" clip-rule="evenodd" d="M12 47H37H12ZM1 31H47H1ZM1 7C1 3.45203 3.61236 1 7 1H41C44.3876 1 47 3.45203 47 7V31.5764C47 35.1244 44.2543 38 40.8667 38H7.13333C3.74569 38 1 35.1244 1 31.5764V7Z" stroke="#D9D9D9" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
  )
}

export default WebDevelopmentIco