export default function Arrow() {
  return(
    <svg width="24" height="12" viewBox="0 0 24 12" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" clip-rule="evenodd" d="M17.9677 0L24 6L17.9677 12L16.9021 10.9401L21.1102 6.73657H0V5.26343H21.1102L16.9021 1.05994L17.9677 0Z" fill="#1E5EE5"/>
    </svg>
  )
}