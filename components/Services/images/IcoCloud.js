export default function IcoCloud() {
  return(
    <svg width="48" height="44" viewBox="0 0 48 44" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M40.95 35.05L47.25 38.2M23.625 24.025L6.3 16.15V35.05L23.625 42.925V24.025ZM23.625 24.025L40.95 16.15V35.05L23.625 42.925V24.025ZM40.95 16.15L23.625 8.27499L6.3 16.15H40.95ZM23.625 8.27499V0.399994V8.27499ZM6.3 35.05L0 38.2L6.3 35.05Z" stroke="#D9D9D9" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
  )
}