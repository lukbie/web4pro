export default function IcoLighthouse() {
  return(
    <svg width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M24 47C36.7025 47 47 36.7025 47 24C47 11.2975 36.7025 1 24 1C11.2975 1 1 11.2975 1 24C1 36.7025 11.2975 47 24 47Z" stroke="#D9D9D9" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M26.828 26.828C26.0736 27.5566 25.0632 27.9598 24.0144 27.9507C22.9656 27.9416 21.9624 27.5209 21.2207 26.7793C20.4791 26.0376 20.0584 25.0344 20.0493 23.9856C20.0402 22.9368 20.4434 21.9264 21.172 21.172C22.734 19.61 37.436 10.572 37.436 10.572C37.436 10.572 28.4 25.266 26.828 26.828Z" stroke="#D9D9D9" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M7 24H10" stroke="#D9D9D9" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M11.98 11.98L14.1 14.1" stroke="#D9D9D9" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M24 7V10" stroke="#D9D9D9" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M41 24H38" stroke="#D9D9D9" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M41.266 39.2C36.197 35.7019 30.1578 33.8831 24 34C17.8422 33.8831 11.8031 35.7019 6.73401 39.2" stroke="#D9D9D9" stroke-miterlimit="10" stroke-linecap="round"/>
    </svg>
  )
}