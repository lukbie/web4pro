export default function WebsiteDesigne(props) {
  return (
    <svg width="41" height="48" viewBox="0 0 41 48" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" clip-rule="evenodd" d="M7 47C3.5203 47 1 44.4797 1 41V13C1 12.0214 1.5611 10.8736 2 10C2.2926 9.4176 3.62593 6.75093 6 2C6.2667 1.4666 6.4036 1 7 1C7.5964 1 7.7333 1.4666 8 2L12 10C12.4389 10.8736 13 12.0214 13 13V41C13 44.4797 10.4797 47 7 47ZM1 13H13H1ZM1 39H13H1ZM21 4C21 2.342 22.2612 1 24 1H37C38.7388 1 40 2.342 40 4V44C40 45.658 38.3388 47 36.6 47H24C22.2612 47 21 45.658 21 44V4Z" stroke="#D9D9D9" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M21 38H27.3M21 11H27.3H21ZM21 20H27.3H21ZM21 29H27.3H21Z" stroke="#D9D9D9" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
  )
}