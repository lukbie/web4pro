import ServiceItem from './ServiceItem'
import Link from "next/link"

export default function ServiceColumn(props) {
  const {title, serviceBig, serviceSmall} = props
  return (
    <div>
      <div className="services__items__title show-for-large">{title}</div>
      <Link href={serviceBig.link}><a>
        <ServiceItem
          title={serviceBig.title}
          subTitle={serviceBig.subtitle}
          icon={serviceBig.icon} 
          bigItem
        />
      </a></Link>
      {/* <Link href={serviceSmall.link}><a>
        <ServiceItem 
          title={serviceSmall.title}
          icon={serviceSmall.icon}
        />
      </a></Link> */}
    </div>
  )
}