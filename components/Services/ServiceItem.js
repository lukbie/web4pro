import Arrow from './images/Arrow'

export default function ServiceItem(props) {
  const { bigItem, icon, title, subTitle } = props
  return (
    <div className={`services__item ${bigItem ? 'services__item--big' : null}`}>
      <div className={`services__item__title ${!bigItem ? ' services__item__title--center' : null}`}>

  {bigItem ? <><div>{ title }</div><div className="icon">{ icon }</div></> : <><div className="icon">{ icon }</div><div>{ title }</div></>} 

      </div>
      {bigItem ?
        <><div className="services__item__subtitle">{ subTitle }</div>
        <div className="services__arrowhide"></div>
        <div className="services__arrow"><Arrow /></div></> 
      : null}
    </div>
  )
}