import './_services.scss'
import WebDevelopmentIco from './images/IcoWebDevelopment'
import WebsiteDesigne from './images/IcoWebsiteDesigne'
import Plus from './Plus'
import ServiceColumn from './ServiceColumn'
import IcoSeo from './images/IcoSeo'
import IcoCloud from './images/IcoCloud'
import IcoEcom from './images/IcoEcom'

const Services = (props) => {
  return (
    <div className="services">
      <div className="services__header container">
        <div className="services__title">Services</div>
        <div className="services__subtitle">All business needs in one place</div>
      </div>
      <div className="services__items container">

        <ServiceColumn 
          title=""
          serviceBig={{
            title: "Web Development",
            subtitle: "Showcase your business & attract more customers",
            icon: <WebDevelopmentIco/>, 
            link: "/services/web-development"
          }}
          serviceSmall={{
            title: "Website design",
            icon: <WebsiteDesigne />,
            link: "/services/website-designe"
          }}
        />

        <Plus/>

        <ServiceColumn 
          title=""
          serviceBig={{
            title: "E-commerce",
            subtitle: "Fast website for optimized google search & user experience",
            icon: <IcoEcom />,
            link: "/services/e-commerce" 
          }}
          serviceSmall={{
            title: "Local SEM & SEO",
            icon: <IcoSeo />,
            link: "/services/web-development"
          }}
        />   

        <Plus/>  

        <ServiceColumn 
          title=""
          serviceBig={{
            title: "Hosting & Maintenance",
            subtitle: "Focus on the fundamentals & cut down on admin time",
            icon: <IcoCloud />,
            link: "/services/hosting-and-maintenance"
          }}
          serviceSmall={{
            title: "Cloud Serwer",
            icon: <IcoCloud />,
            link: "/services/web-development"
          }}
        />   

      </div>
    </div>
  )
}

export default Services