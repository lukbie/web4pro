import React, { useState, forwardRef, useImperativeHandle } from 'react'
import './_estimate.scss'
import TextField from '@material-ui/core/TextField';

const Estimate = forwardRef((props, ref) => {
  const [fullScreen, setFullScreen] = useState(false)
  const showEstimate = () => {
    setFullScreen(true)
  }
  const hideEstimate = () => {
    setFullScreen(false)
  }
  useImperativeHandle(ref, () => {
    return {
      showEstimate: showEstimate
    }
  })

  const script = {__html: "<script>var mndFileds=new Array('Last_Name','Email','Description');var fldLangVal=new Array('Full name','Email','Tell us about your project');var name='';var email=''; function disableSubmitwhileReset(){var e=document.getElementById(\"formsubmit\");null!==document.getElementById(\"privacyTool\")||null!==document.getElementById(\"consentTool\")?(e.disabled=!0,e.style.opacity=\"0.5;\"):e.removeAttribute(\"disabled\")}function checkMandatory(){var e,t=mndFileds.length;for(e=0;e<t;e++){var i=document.forms.CMIGNITEWebToContacts[mndFileds[e]];if(i){if(0===i.value.replace(/^\s+|\s+$/g,\"\").length)return\"file\"===i.type?(alert(\"Please select a file to upload.\"),i.focus(),!1):(alert(fldLangVal[e]+\" cannot be empty.\"),i.focus(),!1);if(\"SELECT\"===i.nodeName){if(\"-None-\"===i.options[i.selectedIndex].value)return alert(fldLangVal[e]+\" cannot be none.\"),i.focus(),!1}else if(\"checkbox\"===i.type&&!1===i.checked)return alert(\"Please accept  \"+fldLangVal[e]),i.focus(),!1;try{\"Last Name\"===i.name&&(name=i.value)}catch(e){}}}return validateFileUpload()}function validateFileUpload(){var e=document.getElementById(\"theFile\"),t=0;if(e){if(e.files.length>3)return alert(\"You can upload a maximum of three files at a time.\"),!1;if(\"files\"in e){var i=e.files.length;if(0!==i){for(var n=0;n<i;n++){var r=e.files[n];\"size\"in r&&(t+=r.size)}if(t>20971520)return alert(\"Total file(s) size should not exceed 20MB.\"),!1}}}}</script>"}

  return (
    <div className={fullScreen ? 'estimate estimate--active' : 'estimate'}>
      {/* <div className="close" onClick={hideEstimate}>
        <svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M13 29.3077L29 13.3077" stroke="black" stroke-width="2" stroke-linecap="square"/>
        <path d="M13 13.3077L29 29.3077" stroke="black" stroke-width="2" stroke-linecap="square"/>
        <path d="M21 41C32.0457 41 41 32.0457 41 21C41 9.9543 32.0457 1 21 1C9.9543 1 1 9.9543 1 21C1 32.0457 9.9543 41 21 41Z" stroke="black" stroke-width="2"/>
        </svg>
      </div>
      <div className="estimate__content">
        <div>
          <div className="estimate__title">Bezpłatna wycena</div>
          <TextField id="standard-basic" label="Twoje imię" color="#1E5EE5" />
          <TextField id="standard-basic" label="Adres e-mail" color="#1E5EE5" />
          <TextField id="standard-basic" label="Numer telefonu (opcjonalnie)" color="#1E5EE5" />
          <TextField id="standard-basic" label="Opisz co potrzebujesz" multiline color="#1E5EE5" />
          <div className="submit"><span className="btn_cta">Wyślij zapytanie</span></div>
          <div className="estimate__email">lub napisz do nas bezpośrednio <a href="">hello@web4pro.pl</a></div>
        </div>
        <div>
          riht
        </div>
      </div> */}
    </div> 
  )
})
 
export default Estimate;