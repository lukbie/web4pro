import './_hero.scss'
import { PopupText } from "react-calendly"

const Hero = (props) => {
  const { title, subtitle, bgImg, displayCta, h1} = props
  return (
    <div className="hero">
      <div className="container">
        <div>
          <div className="hero__title">{h1 ? <h1>{title}</h1> : title}</div>
          <div className="hero__subtitle">{subtitle}</div>
          {/* {displayCta ? <div className="hero__cta"><span onClick={props.showEstimate} className="btn_cta">Tell us about your project</span></div> : null} */}
          {displayCta ? <div className="hero__cta"><span className="btn_cta"><PopupText text="Tell us about your project" url="https://calendly.com/cleverwebsite/30min?primary_color=1a31f4&month=2020-09&back=1" /></span></div> : null}
        </div>
        <div className="hero__img">{bgImg}</div>
      </div>
    </div>
  )
}

export default Hero