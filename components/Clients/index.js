import './_clients.scss'
import { ReactSVG } from 'react-svg'

const Clients = (props) => {
  return (
    <div className="clients">
      <div className="container">
        <div className="clients__title">Our technology stack:</div>
        <div className="clients__logos">
          <img src="/images/timber.png" alt="timber" />
          <img src="/images/react.png" alt="react" />
          <img src="/images/nodejs.png" alt="nodejs" />
          <img src="/images/wordpress.png" alt="wordpress" />
          <img src="/images/sass.png" alt="sass" />
          <ReactSVG src="/images/html5.svg" />
          <ReactSVG src="/images/css3.svg" />
          <img src="/images/php7.jpg" alt="php7" />
          <ReactSVG src="/images/bedrock.svg" />
        </div>
      </div>
    </div>
  )
}

export default Clients