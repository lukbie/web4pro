import './_footer.scss'
import Link from 'next/link'

const Footer = (props) => {
  return (
    <div className="footer">
      <div className="container">
        <div>
          <div style={{marginBottom: 11}}>
            <div style={{fontSize: 16, letterSpacing: -1, fontWeight: '300', marginBottom: 8}}>Get in touch:</div>
            <div><a style={{fontSize: 24, fontWeight: 'bold', letterSpacing: -1, color: '#3A3A47', textDecoration: 'none'}} href="mailto:ali@cleverwebsite.co.uk">ali@cleverwebsite.co.uk</a></div>
          </div>
          {/* <div>
            <div style={{fontSize: 14, letterSpacing: -1, fontWeight: '300', marginBottom: 5}}>lub zadzwoń</div>
            <div style={{fontSize: 22, fontWeight: 'bold', letterSpacing: -1, color: '#3A3A47', textDecoration: 'none'}}>0 801 478 194</div>
          </div> */}
        </div>
        <div>
          <div className="footer__title">Office</div>
          6 Tay Street<br/>
          Edinburgh, Scotland<br/> 
          EH11 1EA
          <div className="footer__about-link"><Link href="/about-us"><a>About Us</a></Link></div>
        </div>
        <div className="footer__service-row">
          <div style={{fontSize: 18, fontWeight: '600', color: '#3A3A47', letterSpacing: 0}}>Services</div>
          <div style={{display: 'flex'}}>
            <div className="footer__services">
              <Link href="/services/web-development"><a>Web Development</a></Link>
              <Link href="/services/hosting-and-maintenance"><a>Hosting & Maintenance</a></Link>
            </div>
            <div className="footer__services">
              <Link href="/services/e-commerce"><a>E-commerce</a></Link>
              <Link href="/services/website-design"><a>Website Design</a></Link>
            </div>
          </div>
        </div>
      </div>
      <div className="footer__copy">
        <div className="container">
          <div>2020 cleverWebsite Ltd</div>
          <div><Link href="/privacy-policy"><a>Privacy Policy</a></Link></div>
        </div>
      </div>
    </div>
  )
}

export default Footer