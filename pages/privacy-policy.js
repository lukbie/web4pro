import Head from 'next/head'
import Header from '../components/Header'
import Footer from '../components/Footer'
import HeroAboutUs from '../components/HeroAboutUs'

const AboutUs = () => {
  return (
    <div className="privacyPolice">
      <Head>
        <title>cleverWebsite - About Us</title>
      </Head>
      <Header />
      <div className="container container--noflex">
        <h2>We value your privacy at cleverWebsite.</h2>
        <p>This privacy policy (“policy”) will help you understand how cleverWebsite (“us”, “we”, “our”) uses and protects the data you provide to us when you visit and use cleverwebsite.co.uk(“website”, “service”). We reserve the right to change this policy at any given time, of which you will be promptly updated. If you want to make sure that you are up to date with the latest changes, we advise you to frequently visit this page.</p>
        <h2>What user data we collect.</h2>
        <p>When you visit the website, we may collect the following data:</p>
        <ul>
          <li>Your IP address.</li>    
          <li>Your contact information and email address.</li>    
          <li>Other information such as interests and preferences.</li>    
          <li>Data profile regarding your online behaviour on our website.</li>    
        </ul>  
        <h2>Why we collect your data.</h2>
        <p>We are collecting your data for several reasons:</p>
        <ul>
          <li>To better understand your needs.</li>
          <li>To improve our services and products.</li>
          <li>To send you promotional emails containing the information we think you will find interesting.</li>
          <li>To contact you to fill out surveys and participate in other types of market research.</li>
          <li>To customize our website according to your online behaviour and personal preferences.</li>
        </ul>
        <h2>Safeguarding and securing the data.</h2>
        <p>cleverWebsite is committed to securing your data and keeping it confidential. cleverWebsite  has done all in its power to prevent data theft, unauthorized access, and disclosure by implementing the latest technologies and software, which help us safeguard all the information we collect online.</p>
        <h2>Our cookie policy.</h2>
        <p>Once you agree to allow our website to use cookies, you also agree to use the data it collects regarding your online behaviour (analyze web traffic, web pages you spend the most time on, and websites you visit). The data we collect by using cookies is used to customize our website to your needs. After we use the data for statistical analysis, the data is completely removed from our systems. Please note that cookies don’t allow us to gain control of your computer in any way. They are strictly used to monitor which pages you find useful and which you do not so that we can provide a better experience for you. If you want to disable cookies, you can do it by accessing the settings of your internet browser. (Provide links for cookie settings for major internet browsers)</p>
        <h2>Links to other websites.</h2>
        <p>Our website contains links that lead to other websites. If you click on these links cleverWebsite is not held responsible for your data and privacy protection. Visiting those websites is not governed by this privacy policy agreement. Make sure to read the privacy policy documentation of the website you go to from our website.</p>
        <h2>Restricting the use of your personal data.</h2>
        <p>At some point, you might wish to restrict the use and collection of your personal data. You can achieve this by doing the following: When you are filling the forms on the website, make sure to check if there is a box which you can leave unchecked if you don’t want to disclose your personal information. If you have already agreed to share your information with us, feel free to contact us via email and we will be more than happy to change this for you. cleverWebsite will not lease, sell or distribute your personal information to any third parties unless we have your permission. We might do so if the law forces us. Your personal information will be used when we need to send you promotional materials if you agree to this privacy policy.</p>

        <h2>Get in touch.</h2>
        <p>For any privacy-related or cookie queries please contact us using our contact page or email us at ali@cleverwebsite.co.uk</p>
      </div>
      <Footer/>
    </div>
  )
}

export default AboutUs
