import React, { useRef } from "react"
import Head from 'next/head'
import Link from 'next/link'
import Header from '../../components/Header'
import Hero from '../../components/Hero'
import Estimate from '../../components/Estimate'
import Cta from '../../components/Cta'
import Footer from '../../components/Footer'
import IconTeam from '../../assets/image/IconTeam'
import IconRWD from '../../assets/image/IconRwd'
import IcoMoney from '../../assets/image/IcoMoney'
import { ReactSVG } from 'react-svg'

const WebDevelopment = () => {
  const ref = useRef(null);
  const handleClick = () => {
    ref.current.showEstimate();
  }
  return (
    <div>
      <Head>
        <title>cleverWebsite - Web Development</title>
      </Head>
      <Estimate ref={ref} />
      <Header showEstimate={handleClick} />
      <div className="service-single">
        <Hero
          showEstimate={handleClick} 
          title={<>Website <span>Design
            </span></>}
          subtitle="Create the website of your dreams together with our creative team. Your web design is the first thing that the audience will see and makes a lasting impression - you paint the vision and we make it happen!"
          bgImg={<div className="header__image show-for-large"> <img srcset="/images/website-designe.jpg" alt="Node.JS development and services by TSH" className="header__image-clip" /></div>}
          displayCta
          h1
        />
        <div className="container container--noflex">
          <p className="subtitle-top"> Our approach</p>
          <h2 className="hasSubtitle">Designs that boost conversion rates</h2>
          <div className="row1323" style={{marginBottom: 60}}>
            <div>
              <p>We help you boost profitability by attracting the audience. Combining visual user experience with technology is extremely important for seamless use of your website. We create websites that tick both boxes – we help you keep visitors on the website and encourage them to explore all areas.</p>
            </div>
            <div>
              <div className="listIcons">
                
                <div className="listIcons__item">
                  <div className="listIcons__item__ico">
                    <ReactSVG src="/images/phone.svg" />
                  </div>
                  <div className="listIcons__item__txt">
                    <p>Mobile First</p>
                    <p>Created for mobile devices –  on average 80% of users access websites through phones</p>
                  </div>
                </div>

                <div className="listIcons__item">
                  <div className="listIcons__item__ico">
                    <IcoMoney/>
                  </div>
                  <div className="listIcons__item__txt">
                    <p>Focus on business</p>
                    <p>Tailored to help you reach business goals</p>
                  </div>
                </div>

                <Link href="/services/web-development"><a>
                  <div className="listIcons__item listIcons__item--link listIcons__item--full">
                    <div className="listIcons__item__ico">
                      <IconTeam/>
                    </div>
                    <div className="listIcons__item__txt">
                      <p>Design Supports Development</p>
                      <p>Performance websites equals profitable websites</p>
                      <p className="link">Read more</p>
                    </div>
                    <div class="services__arrowhide"></div>
                    <div class="services__arrow"><svg width="24" height="12" viewBox="0 0 24 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M17.9677 0L24 6L17.9677 12L16.9021 10.9401L21.1102 6.73657H0V5.26343H21.1102L16.9021 1.05994L17.9677 0Z" fill="#1E5EE5"></path></svg></div>  
                  </div>
                </a></Link>

              </div>
            </div>
          </div>
        </div>
        <Cta showEstimate={handleClick}/>
        {/* <Services small/> */}
      </div>
      <Footer/>
    </div>
  )
}

export default WebDevelopment
