import React, { useRef } from "react"
import Head from 'next/head'
import Link from 'next/link'
import Header from '../../components/Header'
import Hero from '../../components/Hero'
import Estimate from '../../components/Estimate'
import Cta from '../../components/Cta'
import Footer from '../../components/Footer'
import { ReactSVG } from 'react-svg'

const WebDevelopment = () => {
  const ref = useRef(null);
  const handleClick = () => {
    ref.current.showEstimate();
  }
  return (
    <div className="">
      <Head>
        <title>cleverWebsite - Web Development</title>
      </Head>
      <Estimate ref={ref} />
      <Header showEstimate={handleClick} />
      <div className="service-single">
        <Hero
          showEstimate={handleClick} 
          title={<>Hosting & <span>Maintenance</span></>}
          subtitle={<><p>Choosing the right <strong>hosting</strong> is extremely important to secure 24/7 access to your website. Our solution is <strong>optimized for search engines</strong> and ensures your website is constantly available to the audience.</p> <p>All of our websites include <strong>security certificate (SSL)</strong> as a standard. We also offer <strong>maintenance packages</strong> tailored to your business needs.</p></>}
          bgImg={<div className="header__image show-for-large"> <img srcset="/images/layer-01@3x.png" alt="Node.JS development and services by TSH" className="" /></div>}
          displayCta
          h1
        />
        <div className="container container--noflex hm">
          <p className="subtitle-top">Our approach</p>
          <h2 className="hasSubtitle">Security and maintenance for peace of mind</h2>
        </div>
        <div className="container table">
          <div>
            <ReactSVG src="/images/server.svg" />
            <div className="table__title">Cloud Server With Global CDNN</div>
            <p>An ultra-fast scalable solution. Content delivered by a server in an end-user location. Used by Netflix and Amazon.</p>
            <div className="table__price">free<span> / 1 year*</span></div>
          </div>
          <div>
            <ReactSVG src="/images/ssl.svg" />
            <div className="table__title">SSL Certificate for Seamless SEO</div>
            <p>Encryption for secure communications - required by Google for SEO, protects your website users.</p>
            <div className="table__price">free<span> / lifetime</span></div>
          </div>
          <div>
            <ReactSVG src="/images/shield.svg" />
            <div className="table__title">Maintenance And Backup</div>
            <p>Keep your website up-to-date and protect the content with regular backup.</p>
            <div className="table__price">£15<span> / month</span></div>
          </div>
          <div>
            <ReactSVG src="/images/users.svg" />
            <div className="table__title">Admin And Copywriting</div>
            <p>Focus on the fundamentals & cut down on admin time.</p>
            <div className="table__price">£30<span> / hour</span></div>
          </div>
        </div>
        <div className="container" style={{marginTop: -100, marginBottom: 50}}>
          <p style={{fontSize: 14}}>*Then £150/year</p>
        </div>
        <Cta showEstimate={handleClick}/>
        {/* <Services small/> */}
      </div>
      <Footer/>
    </div>
  )
}

export default WebDevelopment
