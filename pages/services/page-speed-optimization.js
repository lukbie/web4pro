import React, { useRef } from "react"
import Head from 'next/head'
import Link from 'next/link'
import Header from '../../components/Header'
import Hero from '../../components/Hero'
import Estimate from '../../components/Estimate'
import Services from '../../components/Services'
import Cta from '../../components/Cta'
import Footer from '../../components/Footer'
import Clients from "../../components/Clients"
import IconTeam from '../../assets/image/IconTeam'
import IconRWD from '../../assets/image/IconRwd'
import IconAward from '../../assets/image/IconAward'
import IconSeo from '../../assets/image/IconSeo'
import IcoMoney from '../../assets/image/IcoMoney'
import { ReactSVG } from 'react-svg'

const WebDevelopment = () => {
  const ref = useRef(null);
  const handleClick = () => {
    ref.current.showEstimate();
  }
  return (
    <div>
      <Head>
        <title>cleverWebsite - Web Development</title>
      </Head>
      <Estimate ref={ref} />
      <Header showEstimate={handleClick} />
      <div className="service-single lighthousePage">
        <Hero
          showEstimate={handleClick} 
          title={<>Page <span>spped optimization</span></>}
          subtitle="Create the website of your dreams together with our creative team. Your web design is the first thing that the customer will see and makes a lasting impression - you paint the vision and we make it happen! "
          bgImg={<div className="header__image"><ReactSVG src="/images/speed.svg" /></div>}
          displayCta
          h1
        />
        {/* <div className="container container--noflex">
          <p className="subtitle-top"> Our approach</p>
          <h2 className="hasSubtitle">Designe raises the conversion rate (CR)</h2>
        </div> */}
        {/* <Cta showEstimate={handleClick}/> */}
        {/* <Services small/> */}
      </div>
      <Footer/>
    </div>
  )
}

export default WebDevelopment
