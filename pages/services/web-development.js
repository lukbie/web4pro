import React, { useRef } from "react"
import Head from 'next/head'
import Link from 'next/link'
import Header from '../../components/Header'
import Hero from '../../components/Hero'
import Estimate from '../../components/Estimate'
import Services from '../../components/Services'
import Cta from '../../components/Cta'
import Footer from '../../components/Footer'
import Clients from "../../components/Clients"
import IconTeam from '../../assets/image/IconTeam'
import IconRWD from '../../assets/image/IconRwd'
import IconAward from '../../assets/image/IconAward'
import IconSeo from '../../assets/image/IconSeo'
import IcoMoney from '../../assets/image/IcoMoney'

const WebDevelopment = () => {
  const ref = useRef(null);
  const handleClick = () => {
    ref.current.showEstimate();
  }
  return (
    <div>
      <Head>
        <title>cleverWebsite - Web Development</title>
      </Head>
      <Estimate ref={ref} />
      <Header showEstimate={handleClick} />
      <div className="service-single">
        <Hero
          showEstimate={handleClick} 
          title={<>Web <span>Development</span></>}
          subtitle="Whether you are looking for a website for your business, organisation or as an individual – we’ve got you covered! We specialise in developing bespoke products based on individual project needs. All of our creations are search engine optimised and include a built-in CRM system for simple website management. "
          bgImg={<div className="header__image show-for-large"> <img srcset="/images/web-development.jpg" alt="Node.JS development and services by TSH" className="header__image-clip" /></div>}
          displayCta
          h1
        />
        <div className="container container--noflex">
          <p className="subtitle-top"> Our approach</p>
          <h2 className="hasSubtitle">Web development done right</h2>
          <div className="row2313">
            <div>
              <div className="listIcons">

                <div className="listIcons__item">
                  <div className="listIcons__item__ico">
                    <IconTeam/>
                  </div>
                  <div className="listIcons__item__txt">
                    <p>Professional</p>
                    <p>Experienced team of front-end and back-end developers</p>
                  </div>
                </div>

                <div className="listIcons__item">
                  <div className="listIcons__item__ico">
                    <IconRWD/>
                  </div>
                  <div className="listIcons__item__txt">
                    <p>Responsive Web Design</p>
                    <p>Websites created according to the best RWD practises</p>
                  </div>
                </div>

                <div className="listIcons__item">
                  <div className="listIcons__item__ico">
                    <IcoMoney/>
                  </div>
                  <div className="listIcons__item__txt">
                    <p>Business-Focused</p>
                    <p>Tailored to your individual needs</p>
                  </div>
                </div>

                <div className="listIcons__item">
                  <div className="listIcons__item__ico">
                    <IconSeo/>
                  </div>
                  <div className="listIcons__item__txt">
                    <p>SEO-Friendly</p>
                    <p>Making your business visible</p>
                  </div>
                </div>
                
                <Link href="/services/website-design"><a>
                  <div className="listIcons__item listIcons__item--link listIcons__item--full">
                    <div className="listIcons__item__ico">
                      <IconAward/>
                    </div>
                    <div className="listIcons__item__txt">
                      <p>Awesome Design</p>
                      <p>Stunning web designs created by award-winning graphic designers</p>
                      <p className="link">Read more</p>
                    </div>
                    <div class="services__arrowhide"></div>
                    <div class="services__arrow"><svg width="24" height="12" viewBox="0 0 24 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M17.9677 0L24 6L17.9677 12L16.9021 10.9401L21.1102 6.73657H0V5.26343H21.1102L16.9021 1.05994L17.9677 0Z" fill="#1E5EE5"></path></svg></div>  
                  </div>
                </a></Link>

              </div>
            </div>
            <div>
              <p>The core of our business is custom web development. During the last 5 years, we have delivered future-proof projects tailored to individual business needs for some of the biggest companies in Poland.</p>
              <p>Our team of skilled developers and designers ensures the final product is free of faults and provides the best experience to website visitors. We are experienced in the newest technologies and can build bespoke functionality extensions to meet your expectations.</p>
            </div>
          </div>
        </div>
        <Clients />
        <Cta showEstimate={handleClick}/>
        {/* <Services small/> */}
      </div>
      <Footer/>
    </div>
  )
}

export default WebDevelopment
