import React, { useRef } from "react"
import Head from 'next/head'
import Link from 'next/link'
import Header from '../../components/Header'
import Hero from '../../components/Hero'
import Estimate from '../../components/Estimate'
import Services from '../../components/Services'
import Cta from '../../components/Cta'
import Footer from '../../components/Footer'
import { ReactSVG } from 'react-svg'

const WebDevelopment = () => {
  const ref = useRef(null);
  const handleClick = () => {
    ref.current.showEstimate();
  }
  return (
    <div>
      <Head>
        <title>cleverWebsite - e-commerce</title>
      </Head>
      <Estimate ref={ref} />
      <Header showEstimate={handleClick} />
      <div className="service-single">
        <Hero
          showEstimate={handleClick} 
          title={<>Smart <span>E-commerce</span></>}
          subtitle="Creating a remarkable e-commerce platform doesn't need to be complicated! We offer solutions that meet your specific needs, are intuitive and allow you to easily manage the chosen platform. Our team analyses each project individually and recommends the best option for your business."
          bgImg={<div className="header__image show-for-large"> <img srcset="/images/ecommerce.jpg" alt="Node.JS development and services by TSH" className="header__image-clip" /></div>}
          displayCta
          h1
        />
        <div className="container container--noflex">
          <p className="subtitle-top">Our approach</p>
          <h2 className="hasSubtitle">Increase sales with an online shop </h2>
        </div>

        <div className="container ecom-solution">
          <div className="ecom-solution__logo">
            <ReactSVG src="/images/shopify-logo-vector.svg" />
          </div>
          <div className="ecom-solution__txt">
            <h3>Easy and fast</h3>
            <p>Shopify is the best solution for a perfect balance between the price and features. Hosting and service are included in a monthly fee – you can forget about downtime and maintenance!</p>
          </div>
        </div>

        <div className="container ecom-solution">
          <div className="ecom-solution__logo">
            <ReactSVG src="/images/woo.svg" />
          </div>
          <div className="ecom-solution__txt">
            <h3>Elastic and scalable</h3>
            <p>WooCommerce is the best choice for a fully customisable online shop. Extended functionality and extra features can be implemented depending on your business needs.</p>
          </div>
        </div>

        <div className="container ecom-solution">
          <div className="ecom-solution__logo">
            <ReactSVG src="/images/localappsvg.svg" />
          </div>
          <div className="ecom-solution__txt">
            <h3>Mobile and local</h3>
            <p>LocalApp has been designed for small businesses selling to the local community. Your own branded application Available on both Google Play and App Store allows customers quick purchases for collection on delivery. No upfront cost, simple monthly subscription.</p>
          </div>
        </div>

        <Cta showEstimate={handleClick}/>
        {/* <Services small/> */}
      </div>
      <Footer/>
    </div>
  )
}

export default WebDevelopment
