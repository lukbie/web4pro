import Head from 'next/head'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import './_style.scss'

import Link from "next/link"
import ServiceItem from '../../components/Services/ServiceItem'
import IcoWebDevelopment from '../../components/Services/images/IcoWebDevelopment'
import IcoWebsiteDesigne from '../../components/Services/images/IcoWebsiteDesigne'
import IcoLighthouse from '../../components/Services/images/IcoLighthouse'
import IcoSeo from '../../components/Services/images/IcoSeo'
import IcoAdmin from '../../components/Services/images/IcoSeo'
import IcoCloud from '../../components/Services/images/IcoCloud'

import {SERVICES} from '../../data/services'


const Services = () => (
  <div>
    <Head>
      <title>cleverWebsite</title>
    </Head>
    <Header />
    <div className="servicesPage services">
      <div className="container container--noflex">
        <h1>Complete development teams</h1>
        <div className="servicesPage__list">

          {SERVICES.map(service => (
            <div>
              <Link href={service.path}><a>
                <ServiceItem title={service.name} subTitle={service.subTitle} icon={service.icon} bigItem/>
              </a></Link>
            </div>
          ))}

          <div>
            <Link href="/services/e-commerce"><a>
              <ServiceItem title="e-commerce" subTitle="super opis" icon={<IcoCloud/>} bigItem/>
            </a></Link>
          </div>

        </div>
      </div>
    </div>
    <Footer/>
  </div>
)

export default Services
