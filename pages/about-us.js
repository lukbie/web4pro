import Head from 'next/head'
import Header from '../components/Header'
import Footer from '../components/Footer'
import HeroAboutUs from '../components/HeroAboutUs'

const AboutUs = () => {
  return (
    <div className="aboutUs">
      <Head>
        <title>cleverWebsite - About Us</title>
      </Head>
      <Header />
      <HeroAboutUs/>
      <div className="container container--noflex">
        <h1 id="sectionStart">About Us</h1>
        <p>Our team has over 50 years of combined experience in web development and design. We had a pleasure of working with many amazing businesses – from multi-million enterprises to local start-ups.</p>
        <p>Our passion for high-class web development enables us to provide the best solutions to our customers, no matter the scale and budget. We pride ourselves in putting our customers first and developing relationships which make them come back.</p>
      </div>
      <Footer/>
    </div>
  )
}

export default AboutUs
