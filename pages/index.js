import React, { useRef } from "react"
import Head from 'next/head'
import Header from '../components/Header'
import Hero from '../components/Hero/index'
import Services from '../components/Services'
import Cta from '../components/Cta'
import Footer from '../components/Footer'
import Estimate from '../components/Estimate'
import HeroImg from '../assets/image/HeroImg'

const Home = () => {
  const ref = useRef(null);
  const handleClick = () => {
    ref.current.showEstimate();
  };

  return(
    <div className="homePage">
      <Head>
        <title>cleverWebsite - Powerful solutions for professionals</title>
        <meta name="description" content="Websites, eCommerce and Mobile Applications that boost your conversion rates and help you meet business goals."/>
      </Head>
      <Estimate ref={ref} />
      <Header showEstimate={handleClick} />
      <main>
       <Hero 
        showEstimate={handleClick} 
        title={<>websites <span>for professionals</span></>}
        subtitle="Created on demand. Ready when you are."
        bgImg={<HeroImg/>}
        displayCta
      />
      <Services/>
      <Cta showEstimate={handleClick}/>
      </main>
      <Footer/>
    </div>
  )
}

export default Home
