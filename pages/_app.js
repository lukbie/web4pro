import React from 'react';
import App from 'next/app';
import "../styles.scss"

class MyApp extends App {
  componentDidMount() {
    history.scrollRestoration = 'manual'
  }
  render() {
    const { Component, pageProps } = this.props
    return (
      <Component {...pageProps} />
    );
  }
}

export default MyApp