import IcoWebDevelopment from '../components/Services/images/IcoWebDevelopment'
import IcoWebsiteDesigne from '../components/Services/images/IcoWebsiteDesigne'
import IcoLighthouse from '../components/Services/images/IcoLighthouse'
import IcoSeo from '../components/Services/images/IcoSeo'
import IcoAdmin from '../components/Services/images/IcoSeo'
import IcoCloud from '../components/Services/images/IcoCloud'

export const SERVICES = [
  {
    name: 'web development',
    icon: <IcoWebDevelopment/>,
    path: '/services/web-development',
    subTitle: 'Showcase your business & attract more customers'
  },
  {
    name: 'website designe',
    icon: <IcoWebsiteDesigne/>,
    path: '/services/website-designe',
    subTitle: 'super opis'
  },
  {
    name: 'lighthouse',
    icon: <IcoLighthouse/>,
    path: '/services/page-speed-optimization',
    subTitle: 'Fast website for optimized google search & user experience'
  },
  {
    name: 'local SEM & SEO',
    icon: <IcoSeo/>,
    path: '/services/seo-sem',
    subTitle: 'super opis'
  },
  {
    name: 'Administracja',
    icon: <IcoSeo/>,
    path: '',
    subTitle: 'Focus on the fundamentals & cut down on admin time'
  },
  {
    name: 'cloud server',
    icon: <IcoCloud/>,
    path: '/services/cloud-server',
    subTitle: 'super opis'
  }
]