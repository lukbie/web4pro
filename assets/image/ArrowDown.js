const ArrowDown = () => {
  return (
    <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
    <g clip-path="url(#clip0)">
    <path d="M7.83299 2.16699L3.99999 5.83299L0.166992 2.16699" stroke="#161616" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    </g>
    <defs>
    <clipPath id="clip0">
    <rect width="8" height="8" fill="white"/>
    </clipPath>
    </defs>
    </svg>
  )
}

export default ArrowDown